# Aero-Nef

This repository contains the code to reproduce some of the experiments presented in the paper [**Aero-Nef: Neural Fields for Rapid Aircraft Aerodynamic Simulations**](https://arxiv.org/abs/2407.19916).

Aero-Nepf leverages Neural Fields to model pressure fields for rapid aerodynamic simulations. The repository includes code to reproduce some of the experiments from our paper, namely on the 2D Transonic Airfoil. Features include handling of unstructured domains, robustness to discretization, and generalization to unseen geometries.


<img src="images/images_repo/11zon_cropped(6).png" width="800" alt="Model Architecture_1"/>

<img src="images/images_repo/11zon_cropped(7).png" width="400" alt="Model Architecture_2"/>




## Installation

Ensure you have Python installed, then clone this repository and install the required packages:

1. **Clone the repository:**
    ```bash
    git clone https://gitlab.com/yourusername/aero-nepf.git
    cd aero-nepf
    ```

2. **Install the requirements:**
    ```bash
    pip install -r requirements.txt
    ```


## Fitting Single 1 Signal

To reproduce the simple signal fitting experiment, using Fourier Feature Networks, run the Notebook:

    ```bash
    demo_notebook_ms.ipynb
    ```
<img src="images/images_repo/signal_fitting.png" width="400" alt="Sample Results"/>


## Transonic Airfoil Experiments

The dataset can be downloaded at:
https://zenodo.org/records/12700680?token=eyJhbGciOiJIUzUxMiJ9.eyJpZCI6IjQyNzI4M2NmLWIwYjktNDc1Ny1hYjA5LTliYjU4YjY4MjFmNCIsImRhdGEiOnt9LCJyYW5kb20iOiI5ZjY5MWIzNWQ5MTRmNGE4ZDdjNmY4ZjI4MTY1NDAyMiJ9._BqW0JKCMiI89PjbTmNOtbvYO6iCBx-hjP4WRPGepV2ufmAlqk_SEmAgbPfqkW9YvjOsh67lHn2jGQ7cg_n1nw


To reproduce the experiments for the Transonic Airfoil, follow these steps:

1. **Navigate to the desired model folder**:
    - `POD` for Proper Orthogonal Decomposition (PCA) and Gaussian Process Regression (GPR)
    - `GUNET` for Graph Unet
    - `INR end-to-end` the end-to-end INR model described in the paper

<img src="images/images_repo/Presentazione senza titolo-1-5.png" width="800" alt="Sample Results for Transonic Airfoil Experiments"/>


2. **Edit the design file** in each model folder as needed to configure the parameters for your experiment. (params.yaml or design.yaml)

3. **Run the main script** in the respective model folder:
    ```bash
    python main.py
    ```

## Citation

If you use the code or data from this repository in your research, please cite our paper as follows:

@misc{catalani2024aeronefneuralfieldsrapid,
      title={Aero-Nef: Neural Fields for Rapid Aircraft Aerodynamics Simulations}, 
      author={Giovanni Catalani and Siddhant Agarwal and Xavier Bertrand and Frederic Tost and Michael Bauerheim and Joseph Morlier},
      year={2024},
      eprint={2407.19916},
      archivePrefix={arXiv},
      primaryClass={cs.CE},
      url={https://arxiv.org/abs/2407.19916}, 
}
