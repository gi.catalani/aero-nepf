import numpy as np
from utils import *
from GPYTorch_template import *
import pod as pod
from dataset import *
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C
from scipy.interpolate import CloughTocher2DInterpolator
from sklearn.preprocessing import normalize
from sklearn import preprocessing
import torch
import gpytorch
import scienceplots
import time
import os

plt.style.use(['science','no-latex'])

# Importing the dataset
data_directory = '/home/dmsm/gi.catalani/Projects/RAE_Transonic/data/'
save_directory = '/home/dmsm/gi.catalani/Projects/RAE_Transonic/'

# Create dataset instance
dataset = TransonicRAE(data_directory, 'Pressure', global_norm=False, add_cond_in=False, num_points=None)
dataset.create_splits(train_ratio=0.9, val_ratio=0.05, test_ratio=0.05, seed=42)
coef_norm = dataset.coef_norm

# Load Airfoil
airfoil = np.load(os.path.join(data_directory, 'airfoil.npy'))
X_airfoil = airfoil[:, 0] - 0.5
Y_airfoil = airfoil[:, 1]

# Extract pressure values from the training dataset
P_train = [data.output.numpy() for data in dataset.train_dataset]
# Extract pressure values from the test dataset
P_test = [data.output.numpy() for data in dataset.test_dataset]

# Prepare the input features (conditions) for training and testing
train_x = torch.tensor([data.cond.numpy() for data in dataset.train_dataset], dtype=torch.float).squeeze(1)
test_x = torch.tensor([data.cond.numpy() for data in dataset.test_dataset], dtype=torch.float).squeeze(1)

# Perform POD decomposition
P_mean = np.mean(P_train, 0)
P_train = P_train - P_mean  # Remove Mean
X = dataset.train_dataset[0].pos[:, 0]
Y = dataset.train_dataset[0].pos[:, 1]
# Assemble Snapshot Matrix
U = pod.SnapMatrix(P_train).assemble()

# Calculate Modes and Plot
Pod_train = pod.POD(U)
phi, singular_values = Pod_train.fit()
phi = normalize(phi, axis=0, norm='l2')
Pod_train.plot(phi, X, Y, number_of_modes_to_plot=3)

# Calculate Coefficients Associated to Training and Testing Snapshots
y_train = Pod_train.transform(U, phi)

U_test = pod.SnapMatrix(P_test - P_mean).assemble()
Pod_test = pod.POD(None)  # Only need to initialize to use transform method
y_test = Pod_test.transform(U_test, phi)

latent_dim = 50
train_y = torch.tensor(y_train[:, :latent_dim], dtype=torch.float)
test_y = torch.tensor(y_test[:, :latent_dim], dtype=torch.float)

train_x_normalized, train_x_mean, train_x_std = normalize_data(train_x)
train_y_normalized, train_y_mean, train_y_std = normalize_data(train_y)

test_x_normalized, _, _ = normalize_data(test_x, train_x_mean, train_x_std)
test_y_normalized, _, _ = normalize_data(test_y, train_y_mean, train_y_std)

# Initialize the model and likelihood
dim_output = train_y.shape[1]  # Number of tasks/output dimensions
likelihood = gpytorch.likelihoods.MultitaskGaussianLikelihood(num_tasks=dim_output)
model = BatchIndependentMultitaskGPModel(train_x_normalized, train_y_normalized, likelihood, dim_output)

optimizer = torch.optim.Adam(model.parameters(), lr=0.1)
mll = gpytorch.mlls.ExactMarginalLogLikelihood(likelihood, model)

model.train()
likelihood.train()

total_epochs = 50  # Set the total number of epochs

# Measure training time
start_time = time.time()

for i in range(total_epochs):
    optimizer.zero_grad()
    output = model(train_x_normalized)
    loss = -mll(output, train_y_normalized)
    loss.backward()
    optimizer.step()
    print(f'Iter {i + 1}/{total_epochs} - Loss: {loss.item():.3f}')

training_time = time.time() - start_time

model.eval()
likelihood.eval()

# Measure inference time
start_inference_time = time.time()

with torch.no_grad(), gpytorch.settings.fast_pred_var():
    observed_pred = likelihood(model(test_x_normalized))
    mean_test = observed_pred.mean * train_y_std + train_y_mean
    lower_test, upper_test = observed_pred.confidence_region()

inference_time = (time.time() - start_inference_time) / len(test_x_normalized)

P_pred = P_mean + np.dot(phi[:, :latent_dim], mean_test.T)
P_pred = P_pred.T
P_pred = P_pred * dataset.coef_norm['std'] + dataset.coef_norm['mean']
P_test = [(p * dataset.coef_norm['std'] + dataset.coef_norm['mean']) for p in P_test]

# Convert P_pred to a list of arrays, each with shape (num_points, 1) for consistency with P_test
P_pred_list = [P_pred[i, :].reshape(-1, 1) for i in range(P_pred.shape[0])]
# Calculate MSE for each sample and store in a list
mse_list = [np.mean((P_pred_list[i] - P_test[i]) ** 2) for i in range(len(P_pred_list))]
result_test = {
    'predictions': P_pred_list,
    'targets': P_test,
    'mse': mse_list,
    'cond': [data.cond.numpy() for data in dataset.test_dataset]  # Assuming cond is already in the correct format
}

mean_mse_total = sum(result_test['mse']) / len(result_test['mse'])
print('Mean mse total:', mean_mse_total)

folder_name = f"train_gp_dimlat{latent_dim}_epochs{total_epochs}"
results_directory = os.path.join(save_directory + 'POD/trainings', folder_name)
os.makedirs(results_directory, exist_ok=True)

# Save the outputs
outputs_save_path = os.path.join(results_directory, 'result_test.pt')
torch.save(result_test, outputs_save_path)

# Save metrics to a text file
metrics_save_path = os.path.join(results_directory, 'metrics.txt')
with open(metrics_save_path, 'w') as f:
    f.write(f'Mean MSE over all samples: {mean_mse_total}\n')
    f.write(f'Total training time (POD + GPR fitting): {training_time} seconds\n')
    f.write(f'Average inference time per sample: {inference_time} seconds\n')
