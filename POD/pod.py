import numpy as np
from scipy.linalg import eigh
import matplotlib.pyplot as plt


class POD:
    def __init__(self, X):
        """
        Initialize the POD model with a given data matrix X already centered.

        Parameters:
        -----------
        X:  Data matrix, where each row is a data point and each column is a feature.

        """
        self.X = X

    def fit(self, n_components=None):
        """
        Compute the POD modes and singular values.

        Parameters:
        -----------
        n_components: int, optional
            Number of POD modes to compute. If not specified, all first M modes will be computed.

        Returns:
        --------
        self: object
            Returns the instance of the object.
        """
        R = self.X @ np.transpose(self.X) #correlation matrix (MxM)
        print('Solving Eigenvalue Problem')
        eigval, V = eigh(R) #SVD
        print('Eigenvalue Problem Solved')
        
        # Sort eigenvalues and eigenvectors from high to low
        # Indices of sorted eigenvalues
        sorted_eig_index = np.flip(np.argsort(np.real(eigval)))

        singular_values = np.flip(eigval[sorted_eig_index])

        # Determine spatial modes and time coefficients #
        # Spatial modes; each column represents a spatial mode
        V = np.real(V[:, sorted_eig_index])
        modes = np.transpose(self.X) @ V

        if n_components:
            modes = modes[:, :n_components]
            singular_values = singular_values[:n_components]

        return modes, singular_values
    
    def plot(self,modes, X, Y, number_of_modes_to_plot=2):
        """ 
        Plot first k modes 
        """
        for i in range(0, number_of_modes_to_plot, 1):
            # Create figure for spatial POD modes 
            plt.figure()
            plt.tricontourf(X,Y, modes[:,i], 200, cmap='jet')
            plt.title('Mode ' + str(i + 1))
            clb = plt.colorbar()
            clb.set_label(r'$\Delta C_{\bar{p}}$',
                          labelpad=-20, y=1.1, rotation=0)
            plt.xlabel(r'$\bar{x}$ [-]')
            plt.ylabel(r'$\bar{y}$ [-]')
            

    def transform(self, X_test, modes):
        """
        Transform a new data matrix using the learned POD modes.

        Parameters:
        -----------
        X_test: numpy array
            Data matrix to project on POD modes, where each column is a data point and each row is a feature.

        Returns:
        --------
        A_time: numpy array
            POD coefficients associated to POD modes for traning or testing snapshots
        """

        A_time = np.matmul(X_test, modes)

        return A_time


class SnapMatrix:
    def __init__(self, labels):
        """
        Initialize the Snapshot Matrix

        Parameters:
        -----------
        labels:  data snapshots
        """
        self.labels = labels
        
    def assemble(self):
        """
        Assemble the snapshot matrix 

        """
        # Snapshot Matrix
        U_shape = np.zeros(self.labels[0].shape)
        U_tilde = np.zeros((len(self.labels), len(U_shape.flatten())))

        for i in range(0, len(self.labels)):

            U_tilde[i, :] = self.labels[i].flatten()

        return U_tilde
