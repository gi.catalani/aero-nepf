
import random
import numpy as np
import matplotlib.pyplot as plt



def ind_train_test(V_train,alpha_train,V_min,V_max,alpha_min,alpha_max,k=5):
    #Find indices where alpha_train is within alpha_min and alpha_max
    alpha_indices = np.where((alpha_train >= alpha_min) & (alpha_train <= alpha_max))[0]

    #Find indices where V_train is between V_min and V_max
    V_indices = np.where((V_train >= V_min) & (V_train <= V_max))[0]

    #Find common indices between alpha_indices and V_indices
    common_indices = np.intersect1d(alpha_indices, V_indices)

    # Randomly select k indices from common_indices
    test_indices = random.sample(list(common_indices), k)

    # Remove the test indices from common_indices to get the training indices
    train_indices = np.setdiff1d(common_indices, test_indices)
    
    return train_indices, test_indices

def plot_pred(X,Y,P_pred,P_true):
    fig, axs = plt.subplots(1, 2, figsize=(10, 5))

    # Plot first subplot
    im2 = axs[0].tricontourf(X, Y, P_pred, 50)
    axs[0].set_xlabel(r'$\bar{x}$ [-]')
    axs[0].set_ylabel(r'$\bar{y}$ [-]')
    axs[0].set_title('Prediction')

    # Plot second subplot
    im1 = axs[1].tricontourf(X, Y, P_true, 50)
    axs[1].set_xlabel(r'$\bar{x}$ [-]')
    axs[1].set_ylabel(r'$\bar{y}$ [-]')
    axs[1].set_title('Ground Truth')

    # Create colorbar
    fig.subplots_adjust(right=0.8)
    cbar_ax = fig.add_axes([0.95, 0.15, 0.05, 0.7])
    clb = fig.colorbar(im2, cax=cbar_ax)
    clb.set_label(r'$C_{\bar{p}}$', labelpad=-20, y=1.1, rotation=0)

    plt.tight_layout()
    plt.show()


def normalize_data(data, mean=None, std=None):
    """
    Normalize data using the provided mean and standard deviation. If mean and std are None,
    they will be computed from the data.

    Args:
        data (torch.Tensor): The data to normalize.
        mean (torch.Tensor, optional): The mean to use for normalization. If None, the mean of `data` is used.
        std (torch.Tensor, optional): The standard deviation to use for normalization. If None, the std of `data` is used.

    Returns:
        torch.Tensor: The normalized data.
        torch.Tensor: The mean used for normalization.
        torch.Tensor: The standard deviation used for normalization.
    """
    if mean is None or std is None:
        mean = data.mean()
        std = data.std() + 1e-6  # Add a small value to avoid division by zero

    normalized_data = (data - mean) / std
    return normalized_data, mean, std
