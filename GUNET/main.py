import torch
torch.cuda.empty_cache()
import numpy as np
import pandas as pd
from tqdm import trange
import copy
import os
import yaml
from torch_geometric.data import DataLoader
import matplotlib.pyplot as plt
from dataset import *
from GUNet import *
from MLP import *
from train import *

# Directories
data_directory = '/home/dmsm/gi.catalani/Projects/RAE_Transonic/data/'
with open('params.yaml', 'r') as f: # hyperparameters of the model
    hparams = yaml.safe_load(f)

path = os.getcwd()
# Create dataset instance
dataset = TransonicRAE(data_directory, 'Pressure', num_points=None)
dataset.create_splits(train_ratio=0.9, val_ratio=0.05, test_ratio=0.05, seed=42)
coef_norm = dataset.coef_norm

print("train", len(dataset.train_dataset))
print("val", len(dataset.val_dataset))
device = 'cuda' if torch.cuda.is_available() else 'cpu'

encoder = MLP(hparams['encoder'], batch_norm = False)
decoder = MLP(hparams['decoder'], batch_norm = False)
model = GUNet(hparams, encoder, decoder)    

model = main_train(device, dataset.train_dataset, dataset.val_dataset, model, hparams, path, val_iter = 5)
