import torch
torch.cuda.empty_cache()
import numpy as np
import pandas as pd
from tqdm import trange
import copy
import matplotlib.pyplot as plt
import scienceplots
plt.style.use(['science','no-latex'])
import os
import yaml
from torch_geometric.data import DataLoader
import matplotlib.pyplot as plt
from dataset import *
from GUNet import *
from MLP import *
from train import *


# Directories
data_directory = '/home/dmsm/gi.catalani/Projects/RAE_Transonic/data/'
current_path = os.getcwd()
model_directory = current_path + '/trainings/training_2024-05-15_15-25-38'
with open(os.path.join(model_directory,'hparams.yaml'), 'r') as f: # hyperparameters of the model
    hparams = yaml.safe_load(f)

device = 'cuda' if torch.cuda.is_available() else 'cpu'


def predict(model_directory, hparams, device, resolutions):
    """
    Predict on the test dataset using the best model saved at model_path for multiple resolutions.
    Now includes graph radius calculation for each test dataset.

    Args:
        test_dataset (Dataset): The test dataset.
        model_directory (str): Directory containing the model and where to save results.
        hparams (dict): Hyperparameters for the model.
        device (str): Device to run the prediction on ('cuda' or 'cpu').
        resolutions (list): List of resolutions to test, including None for full resolution.

    Returns:
        None
    """
    encoder = MLP(hparams['encoder'], batch_norm=False)
    decoder = MLP(hparams['decoder'], batch_norm=False)
    model = GUNet(hparams, encoder, decoder)
    model.load_state_dict(torch.load(model_directory + '/bestmodel.pt', map_location=device))
    model = model.to(device)
    model.eval()

    results = {}
    for res in resolutions:
        dataset = TransonicRAE(data_directory, 'Pressure', num_points=res)
        dataset.create_splits(train_ratio=0.9, val_ratio=0.05, test_ratio=0.05, seed=42)
        coef_norm = dataset.coef_norm
        
        # Prepare Test Dataset
        test_dataset_sampled = []
        for data in dataset.test_dataset:
            data_sampled = data.clone()
            data_sampled.edge_index = nng.radius_graph(x=data_sampled.pos.to(device), r=hparams['r'], loop=True, max_num_neighbors=int(hparams['max_neighbors'])).cpu()
            test_dataset_sampled.append(data_sampled)  

        # Prepare the DataLoader
        test_loader = DataLoader(test_dataset_sampled, batch_size=1, shuffle=False)

        predictions, targets, mse_list = [], [], []
        start_time = time.time()
        with torch.no_grad():
            for data in test_loader:
                data = data.to(device)
                output = model(data)
                pred_unnorm = output.cpu().numpy() * coef_norm['std_out'] + coef_norm['mean_out']
                target_unnorm = data.y.cpu().numpy() * coef_norm['std_out'] + coef_norm['mean_out']
                mse = np.mean((pred_unnorm - target_unnorm) ** 2)
                predictions.append(pred_unnorm)
                targets.append(target_unnorm)
                mse_list.append(mse)

        end_time = time.time()
        total_samples = len(test_dataset_sampled)
        prediction_time_per_sample = (end_time - start_time) / total_samples
        mean_mse = np.mean(mse_list)

        results[res] = {
            'Mean MSE': mean_mse,
            'Prediction time per sample': prediction_time_per_sample
        }

        # Save results for full resolution specifically
        if res is None:
            full_results = {
                'inputs': [d.pos.cpu().numpy() for d in test_dataset_sampled],  # Example for pos
                'predictions': predictions,
                'targets': targets,
                'mse': mse_list
            }
            np.save(os.path.join(model_directory, 'predictions.npy'), full_results)

    # Save metrics for all resolutions
    with open(os.path.join(model_directory, 'metrics.txt'), 'w') as f:
        for res, metrics in results.items():
            f.write(f'Resolution: {res if res is not None else "Full"}\n')
            f.write(f'Mean MSE: {metrics["Mean MSE"]}\n')
            f.write(f'Prediction time per sample: {metrics["Prediction time per sample"]} seconds\n')
            f.write('\n')

# Example usage
resolutions = [500, 1000, 2000, 5000, 10000, 20000, None]
predictions = predict( model_directory, hparams, device, resolutions)


