import numpy as np
import pyvista as pv
import os.path as osp
import os
import torch
import json
import matplotlib.pyplot as plt
from tqdm import tqdm
from torch.utils.data import Dataset
import torch
import numpy as np
from torch_geometric.data import Data
import os
from scipy.spatial import Delaunay

class TransonicRAE(Dataset):
    def __init__(self, data_directory, target_field, num_points=None, coef_norm=None):
        super(TransonicRAE, self).__init__()
       
        self.graph_dataset = []
        self.coef_norm = coef_norm
        self.num_points = num_points

        print("Processing dataset...")
        self.process_data(data_directory, target_field)
        
    def process_data(self, data_directory, target_field):

        print('Loading raw data')
        db_random = np.load(os.path.join(data_directory, 'db_random.npy'), allow_pickle=True).item()
        db_cyc = np.load(os.path.join(data_directory, 'db_cyc.npy'), allow_pickle=True).item()
        
        # Merge db_random and db_cyc
        db = {key: np.concatenate((db_random[key], db_cyc[key]), axis=0) for key in ['Pressure','Xcoordinate','Ycoordinate','Vinf','Alpha','idx']}
        print('Raw data Loaded, normalizing data')
    

        self.coef_norm = {'mean_in': None, 'std_in': None,'mean_out': None, 'std_out': None, 'min': None, 'max': None} 
        mean_out = db[target_field].mean()
        std_out = db[target_field].std()
        db[target_field] = (db[target_field]- mean_out)/std_out
        self.coef_norm['mean_out'] = mean_out
        self.coef_norm['std_out'] = std_out

         # Normalize condition data (Vinf and Alpha)
        cond_data = np.stack([db['Alpha'], db['Vinf']/347], axis=1)  # Normalize Vinf by 347
        mean_in = cond_data.mean(axis=0)
        std_in = cond_data.std(axis=0)
        cond_data = (cond_data - mean_in) / std_in
        self.coef_norm['mean_in'] = mean_in
        self.coef_norm['std_in'] = std_in
        

        for idx in tqdm(range(len(db['idx']))):
            if db['Vinf'][idx] / 347 >= 0.2:
                X_coord = 2*(db['Xcoordinate'][idx] - db['Xcoordinate'][idx].min()) / (db['Xcoordinate'][idx].max() - db['Xcoordinate'][idx].min()) - 1
                Y_coord = 2*(db['Ycoordinate'][idx] - db['Ycoordinate'][idx].min()) / (db['Ycoordinate'][idx].max() - db['Ycoordinate'][idx].min()) - 1
                pos = torch.tensor(np.stack((X_coord, Y_coord), axis=1), dtype=torch.float)
                output = torch.tensor(db[target_field][idx], dtype=torch.float).unsqueeze(-1)
                cond = torch.tensor(cond_data[idx], dtype=torch.float)
                cond = cond.repeat(pos.shape[0], 1)  # Repeat cond to match pos size

                if self.num_points is not None and pos.shape[0] > self.num_points:
                    subsample_indices = np.random.choice(pos.shape[0], self.num_points, replace=False)
                    pos = pos[subsample_indices]
                    output = output[subsample_indices]
                    cond = cond[subsample_indices]
                    
                # Concatenate pos and cond for x
                x = torch.cat([pos, cond], dim=1)
                
                self.graph_dataset.append(Data(x=x, pos=pos, y=output))

    
    
    def create_splits(self, train_ratio=0.8, val_ratio=0.1, test_ratio=0.1, seed=42):
        if seed is not None:
            np.random.seed(seed)
        
        total_samples = len(self.graph_dataset)
        indices = np.arange(total_samples)
        np.random.shuffle(indices)
        
        train_end = int(train_ratio * total_samples)
        val_end = train_end + int(val_ratio * total_samples)
        
        train_indices = indices[:train_end]
        val_indices = indices[train_end:val_end]
        test_indices = indices[val_end:]
        
        self.train_dataset = [self.graph_dataset[i] for i in train_indices]
        self.val_dataset = [self.graph_dataset[i] for i in val_indices]
        self.test_dataset = [self.graph_dataset[i] for i in test_indices]


    def __getitem__(self, index):

        return self.graph_dataset[index]

    def __len__(self):
        return len(self.graph_dataset)
    



if __name__ == "__main__":
    data_directory = 'H:/Projects_Catalani/RAE_Transonic/data/'
    save_directory = 'H:/Projects_Catalani/RAE_Transonic/data/'
    batch_size = 32
    target_field = 'Pressure'
    # Create dataset objects
    train_dataset_obj = TransonicRAE(data_directory, target_field, add_cond_in=False,num_points=5000)
    
    idx=100
    # Extract x and y from the dataset
    graph = train_dataset_obj.graph_dataset[idx]
    # Create the scatter plot
    plt.figure()
    plt.scatter(graph.input[:, 0].numpy(), graph.input[:, 1].numpy(), c=graph.output[:, 0].numpy(), cmap='viridis',s = 0.5)
    plt.colorbar(label='y[:, 2] value')
    plt.xlabel('x[:, 0]')
    plt.ylabel('x[:, 1]')
    plt.title(f'Scatter plot for dataset index {idx}')
    plt.show()


