import random
import numpy as np
import os
import matplotlib.pyplot as plt
import time, json
import datetime
import torch
import torch.nn as nn
import torch_geometric.nn as nng
from torch_geometric.loader import DataLoader
from tqdm import tqdm
from pathlib import Path
import os.path as osp
import yaml


def get_nb_trainable_params(model):
   '''
   Return the number of trainable parameters
   '''
   model_parameters = filter(lambda p: p.requires_grad, model.parameters())
   return sum([np.prod(p.size()) for p in model_parameters])

def train(device, model, train_loader, optimizer, scheduler):
    model.train()
    avg_loss_per_var = torch.zeros(4, device = device)
    avg_loss = 0
    iter = 0
    
    for data in train_loader:
        data_clone = data.clone()
        data_clone = data_clone.to(device)          
        optimizer.zero_grad()  
        out = model(data_clone)
        targets = data_clone.y
        loss_criterion = nn.MSELoss(reduction = 'none')
        loss_per_var = loss_criterion(out, targets).mean(dim = 0)
        total_loss = loss_per_var.mean()
        total_loss.backward()
        
        optimizer.step()
        scheduler.step()
        avg_loss_per_var += loss_per_var
        avg_loss += total_loss

        iter += 1

    return avg_loss.cpu().data.numpy()/iter, avg_loss_per_var.cpu().data.numpy()/iter

@torch.no_grad()
def test(device, model, test_loader):
    model.eval()
    avg_loss_per_var = np.zeros(4)
    avg_loss = 0
    iter = 0

    for data in test_loader:        
        data_clone = data.clone()
        data_clone = data_clone.to(device)
        out = model(data_clone)       

        targets = data_clone.y
        loss_criterion = nn.MSELoss(reduction = 'none')
        
        loss_per_var = loss_criterion(out, targets).mean(dim = 0)
        loss = loss_per_var.mean()
        
        avg_loss_per_var += loss_per_var.cpu().numpy()
        avg_loss += loss.cpu().numpy()
        iter += 1
    
    return avg_loss/iter, avg_loss_per_var/iter

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def main_train(device, train_dataset, val_dataset, Net, hparams, path, val_iter = 10):
    '''
        Args:
        device (str): device on which you want to do the computation.
        train_dataset (list): list of the data in the training set.
        val_dataset (list): list of the data in the validation set.
        Net (class): network to train.
        hparams (dict): hyper parameters of the network.
        path (str): where to save the trained model and the figures.
        criterion (str, optional): chose between 'MSE', 'MAE', and 'MSE_weigthed'. The latter is the volumetric MSE plus the surface MSE computed independently. Default: 'MSE'.
        reg (float, optional): weigth for the surface loss when criterion is 'MSE_weighted'. Default: 1.
        val_iter (int, optional): number of epochs between each validation step. Default: 10.
        name_mod (str, optional): type of model. Default: 'GraphSAGE'.
    '''
    

    # Check for a restart and attempt to load the model state if applicable
    if hparams.get('restart', False):
        training_path = hparams['training_path']
        restart_path = training_path + '/bestmodel.pt'  # Adjust as needed
        print(f"Restarting training from {restart_path}")
        Net.load_state_dict(torch.load(restart_path, map_location=device))
        # Adjust the starting epoch based on your tracking or continue from the previous epoch count
    else:    
        # Generate a unique folder for this training session
        timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        training_path = os.path.join(path, 'trainings', f'training_{timestamp}')
        Path(training_path).mkdir(parents=True, exist_ok=True)
        print("Restart path does not exist. Starting training from scratch.")

    model = Net.to(device)
    optimizer = torch.optim.Adam(model.parameters(), lr = hparams['lr'])
    lr_scheduler = torch.optim.lr_scheduler.OneCycleLR(
            optimizer,
            max_lr = hparams['lr'],
            total_steps = (len(train_dataset) // hparams['batch_size'] + 1) * hparams['nb_epochs'],
        )
    val_loader = DataLoader(val_dataset, batch_size = 1)
    start = time.time()

    train_losses, val_losses = [], []
    best_val_loss = np.inf


    pbar_train = tqdm(range(hparams['nb_epochs']), position=0)
    for epoch in pbar_train:        
        train_dataset_sampled = []
        for data in train_dataset:
            data_sampled = data.clone()
            idx = random.sample(range(data_sampled.x.size(0)), hparams['subsampling'])
            idx = torch.tensor(idx)

            data_sampled.pos = data_sampled.pos[idx]
            data_sampled.x = data_sampled.x[idx]
            data_sampled.y = data_sampled.y[idx]
            
            data_sampled.edge_index = nng.radius_graph(x = data_sampled.pos.to(device), r = hparams['r'], loop = True, max_num_neighbors = int(hparams['max_neighbors'])).cpu()
            train_dataset_sampled.append(data_sampled)
        train_loader = DataLoader(train_dataset_sampled, batch_size = hparams['batch_size'], shuffle = True)
        del(train_dataset_sampled)

        train_loss, _ = train(device, model, train_loader, optimizer, lr_scheduler)
        train_losses.append(train_loss)
        print('Train Loss', str(round(train_loss,3)))     

        
  
        if epoch%val_iter == val_iter - 1 or epoch == 0:
            val_dataset_sampled = []
            for data in val_dataset:
                data_sampled = data.clone()
                idx = random.sample(range(data_sampled.x.size(0)), hparams['subsampling'])
                idx = torch.tensor(idx)

                data_sampled.pos = data_sampled.pos[idx]
                data_sampled.x = data_sampled.x[idx]
                data_sampled.y = data_sampled.y[idx]
               
                data_sampled.edge_index = nng.radius_graph(x = data_sampled.pos.to(device), r = hparams['r'], loop = True, max_num_neighbors = int(hparams['max_neighbors'])).cpu()
                
                val_dataset_sampled.append(data_sampled)
            val_loader = DataLoader(val_dataset_sampled, batch_size = 1, shuffle = True)
            del(val_dataset_sampled)

            val_loss, _,  = test(device, model, val_loader)
            val_losses.append(val_loss)
            del(val_loader)
            print('Val Loss',  str(round(val_loss,3))) 
            if val_loss < best_val_loss:
                best_val_loss = val_loss
                torch.save(model.state_dict(), os.path.join(training_path, 'bestmodel.pt'))
                print(f"New best model saved with val loss: {best_val_loss}")


    # Plotting training and validation losses
    plt.figure()
    plt.plot(train_losses, label='Train Loss')
    plt.plot(np.arange(0, len(train_losses), len(train_losses)/len(val_losses)), val_losses, label='Val Loss')
    plt.yscale('log')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig(os.path.join(training_path, 'loss_vs_val.png'))
    plt.close()

    # Saving hyperparameters to a YAML file
    with open(os.path.join(training_path, 'hparams.yaml'), 'w') as file:
        yaml.dump(hparams, file, Dumper=yaml.SafeDumper)

    print(f"Training completed. Best validation loss: {best_val_loss}")
    print(f"Results saved in: {training_path}")
                
            
    
  
    
