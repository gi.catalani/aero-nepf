import torch
from models import ModulatedFourierFeatures, MultiScaleModulatedFourierFeatures, VanillaMLP


def graph_training_step(
    func_rep,
    graph,
    latent_cond = True,
    return_reconstructions=False,
):
    """

    Args:
        coordinates (torch.Tensor): Shape (batch_size, *, coordinate_dim). Note this
            _must_ have a batch dimension.
        features (torch.Tensor): Shape (batch_size, *, feature_dim). Note this _must_
            have a batch dimension.
    """

    
    func_rep.zero_grad()
    
    conditions = graph.cond
    coords = graph.input
    features = graph.output

    loss = 0

    with torch.set_grad_enabled(True):
        if latent_cond:
            features_recon = func_rep.modulated_forward(coords, conditions)
        else:
            features_recon = func_rep(coords)

        loss = ((features_recon - features) ** 2).mean()

    outputs = {"loss": loss}

    if return_reconstructions:
        outputs["reconstructions"] = features_recon
        
    return outputs


NAME_TO_CLASS = {
    "fourier_features": ModulatedFourierFeatures,
    "ms_fourier_features": MultiScaleModulatedFourierFeatures,
    "vanilla_mlp": VanillaMLP
}


def create_inr_instance(cfg, input_dim=1, output_dim=1, device="cuda"):
    device = torch.device(device)
    
    if cfg.inr.model_type == "fourier_features":
        inr = ModulatedFourierFeatures(
            input_dim=input_dim,
            output_dim=output_dim,
            num_frequencies=cfg.inr.num_frequencies,
            latent_dim=cfg.inr.latent_dim,
            width=cfg.inr.hidden_dim,
            depth=cfg.inr.depth,
            modulate_scale=cfg.inr.modulate_scale,
            modulate_shift=cfg.inr.modulate_shift,
            frequency_embedding=cfg.inr.frequency_embedding,
            include_input=cfg.inr.include_input,
            scale=cfg.inr.scale,
            max_frequencies=cfg.inr.max_frequencies,
            base_frequency=cfg.inr.base_frequency,
        ).to(device)
    
    elif cfg.inr.model_type == "ms_fourier_features":
        inr = MultiScaleModulatedFourierFeatures(
            input_dim=input_dim,
            output_dim=output_dim,
            num_frequencies=cfg.inr.num_frequencies,
            latent_dim=cfg.inr.latent_dim,
            width=cfg.inr.hidden_dim,
            depth=cfg.inr.depth,
            include_input=cfg.inr.include_input,
            scales=cfg.inr.scale,
            max_frequencies=cfg.inr.max_frequencies,
            base_frequency=cfg.inr.base_frequency,
        ).to(device)

    elif cfg.inr.model_type == "vanilla_mlp":
        inr = VanillaMLP(
            input_dim=input_dim,
            output_dim=output_dim,
            width=cfg.inr.hidden_dim,
            depth=cfg.inr.depth,
        ).to(device)

    else:
        raise NotImplementedError(f"No corresponding class for {cfg.inr.model_type}")

    return inr


# Function to load a model
def load_model(model_weights, cfg,input_dim, output_dim):
    # ... load the INR model from model_path using the configuration cfg
    # load inr weights
    
    inr_in = create_inr_instance(
        cfg, input_dim=input_dim, output_dim=output_dim, device="cuda"
    )
    inr_in.load_state_dict(model_weights)
    inr_in.eval()

    return inr_in
