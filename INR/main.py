import os
import matplotlib.pyplot as plt
import sys
import yaml
from pathlib import Path
from pickletools import OpcodeInfo
import time
sys.path.append(str(Path(__file__).parents[1]))
from tqdm import tqdm
import hydra
import numpy as np
import torch
import torch.nn as nn
from omegaconf import DictConfig, OmegaConf

from utils import create_inr_instance,load_model
from utils import graph_training_step as training_step
from dataset import *
from torch_geometric.loader import DataLoader



@hydra.main(config_path="", config_name="design.yaml")
def main(cfg: DictConfig) -> None:

    torch.set_default_dtype(torch.float32)

    # optim
    batch_size = cfg.optim.batch_size
    lr_inr = cfg.optim.lr_inr 
    epochs = cfg.optim.epochs
    num_points = cfg.optim.num_points if cfg.inr.target_field !='normals' else None

    # inr
    latent_dim = cfg.inr.latent_dim
    target_field = cfg.inr.target_field
    add_cond_in = False if cfg.inr.add_cond_in is None else cfg.inr.add_cond_in
    global_norm = False if cfg.inr.global_norm is None else cfg.inr.global_norm
    latent_cond = False if cfg.inr.model_type =='vanilla_mlp' else True

    data_directory = cfg.data_directory
    print(data_directory)
    # Create dataset instance
    dataset = TransonicRAE(data_directory, target_field, global_norm=global_norm,add_cond_in=add_cond_in, num_points=num_points)
    dataset.create_splits(train_ratio=0.9, val_ratio=0.05, test_ratio=0.05, seed=42)
    coef_norm = dataset.coef_norm


    print("train", len(dataset.train_dataset))
    print("val", len(dataset.val_dataset))

    print('num points', len(dataset.train_dataset[0].input))

    ntrain = len(dataset.train_dataset)
    nval = len(dataset.val_dataset)
    ntest = len(dataset.test_dataset)

    inr_in = create_inr_instance(
        cfg, input_dim=4, output_dim=1, device="cuda"
    )
    

    optimizer_in = torch.optim.AdamW(
        [
            {"params": inr_in.parameters()},
        ],
        lr=lr_inr,
        weight_decay=0,
    )
    
    # Improved folder naming with timestamp and all output target fields
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    folder_name = f"training_result_{timestamp}" 
    dir = os.path.dirname(__file__)
    results_directory = os.path.join(dir + '/trainings', folder_name)
    os.makedirs(results_directory, exist_ok=True)
    cfg_save_path = os.path.join(results_directory, 'config.yaml')
    # Convert OmegaConf DictConfig to a native Python dictionary
    cfg_dict = OmegaConf.to_container(cfg, resolve=True)  # Set `resolve=True` to resolve all interpolations
    with open(cfg_save_path, 'w') as file:
        yaml.dump(cfg_dict, file, default_flow_style=False)

    # Generate run name
    run_name = f"{cfg.inr.model_type}_nfr{cfg.inr.num_frequencies}_scale{cfg.inr.scale}_{target_field}_nl{cfg.inr.depth}_np{num_points}_epochs{epochs}_lr{lr_inr}_ldim{latent_dim}_condin{add_cond_in}_gnorm{global_norm}"
   
    best_loss = np.inf

    train_loss_history = []
    test_loss_history = []

    # Check if restart training
    if cfg.restart_training:
        if os.path.exists(cfg.saved_model_path):
            checkpoint = torch.load(cfg.saved_model_path)
            inr_in.load_state_dict(checkpoint["inr_in"])
            optimizer_in.load_state_dict(checkpoint["optimizer_inr_in"])
            start_epoch = checkpoint["epoch"] + 1
            print(f"Restarting training from epoch {start_epoch}")
        else:
            print("Saved model not found. Starting training from scratch.")
            start_epoch = 0
    else:
        start_epoch = 0
    
    start_time = time.time()
    for step in tqdm(range(start_epoch, epochs)):
        fit_train_mse_in = 0
        fit_test_mse_in = 0
        test_loss_in  = 1
        # Initialize lists for loss history
        
        use_rel_loss = step % 10 == 0
        use_pred_loss = step % 5 == 0
        step_show = step % 50 == 0

        # Prepare the training dataset for the new epoch
        # Subsample the training dataset
        train_subsample_dataset = subsample_dataset(dataset.train_dataset, num_points)
        train_loader = DataLoader(train_subsample_dataset, batch_size=batch_size, shuffle=True)
        print('Num points for training', len(train_subsample_dataset[0].input))
        for substep,graph in enumerate(
            train_loader
        ):  
            n_samples = len(graph)
            inr_in.train()
            graph.modulations = torch.zeros(batch_size,latent_dim)

            graph = graph.cuda()
            

            outputs = training_step(
                inr_in,
                graph,
                latent_cond=latent_cond,
                return_reconstructions=False,
            )

            optimizer_in.zero_grad()
            outputs["loss"].backward(create_graph=False)
            nn.utils.clip_grad_value_(inr_in.parameters(), clip_value=1.0)
            optimizer_in.step()
            loss = outputs["loss"].cpu().detach()
            fit_train_mse_in += loss.item() * n_samples
            
            

        train_loss_in = fit_train_mse_in / (ntrain)
        if global_norm:
            train_loss_in = train_loss_in*(coef_norm['max'] - coef_norm['min'])**2

        train_loss_history.append({'epoch': step, 'loss': train_loss_in})
        print('Train Loss', train_loss_in)
       

        if use_pred_loss:
            # Prepare the training dataset for the new epoch
            test_subsample_dataset = subsample_dataset(dataset.test_dataset, num_points)
            test_loader = DataLoader(test_subsample_dataset, batch_size=batch_size, shuffle=True)
            for substep,graph in enumerate(test_loader):  
                n_samples = len(graph)
                inr_in.train()
                
                graph.modulations = torch.zeros(batch_size,latent_dim)
                graph = graph.cuda()
                

                outputs = training_step(
                    inr_in,
                    graph,
                    latent_cond=latent_cond,
                    return_reconstructions=False,
                )
                loss = outputs["loss"].cpu().detach()
                fit_test_mse_in += loss.item() * n_samples
              
            test_loss_in = fit_test_mse_in / (ntest)
            if global_norm:
               test_loss_in = test_loss_in*(coef_norm['max'] - coef_norm['min'])**2
            test_loss_history.append({'epoch': step, 'loss': test_loss_in})
            print('Test Loss', test_loss_in)

            plt.figure()
            plt.plot([p['epoch'] for p in train_loss_history], [p['loss'] for p in train_loss_history], label='Train Loss')
            plt.plot([p['epoch'] for p in test_loss_history], [p['loss'] for p in test_loss_history], label='Test Loss')
            plt.xlabel('Epoch')
            plt.ylabel('Loss')
            plt.yscale('log')
            plt.title('Training and Test Loss')
            plt.legend()
            plt.savefig(os.path.join(results_directory, f"{run_name}_loss_plot.png"))
            plt.close()
        
        if test_loss_in < best_loss:
            best_loss = test_loss_in
            
            torch.save(
                {
                    "cfg": cfg,
                    "epoch": step,
                    "inr_in": inr_in.state_dict(),
                    "optimizer_inr_in": optimizer_in.state_dict(),
                },
                os.path.join(results_directory, f"{run_name}.pt"),
            )

    training_time = time.time() - start_time
    inr_model = load_model(torch.load(os.path.join(results_directory, f"{run_name}.pt"))["inr_in"], cfg, 4, 1)
    inr_model.eval()
    
    # Modified inference section for variable resolutions
    resolutions = [500, 1000, 2000, 5000, 10000, 20000, None]
    metrics_path = os.path.join(results_directory, 'result_metrics.txt')
    with open(metrics_path, 'w') as file:
        file.write(f"Training Time: {training_time} seconds\n")
        file.write("Resolution, Inference Time per Sample, Average MSE\n")
        for num_points in resolutions:
            # Load dataset with the current resolution
            dataset = TransonicRAE(data_directory, target_field, global_norm=global_norm, add_cond_in=add_cond_in, num_points=num_points)
            dataset.create_splits(train_ratio=0.9, val_ratio=0.05, test_ratio=0.05, seed=42)
            test_dataset = dataset.test_dataset
            test_loader = DataLoader(test_dataset, batch_size=1, shuffle=False)

            # Conditionally reinitializing result_test dictionary for full resolution
            if num_points is None:
                result_test = {'predictions': {}, 'targets' : {}, 'mse' : {}, 'cond' : {}}
                result_test['predictions'][target_field] = []
                result_test['targets'][target_field] = []
                result_test['mse'][target_field] = []
                result_test['cond'][target_field] = []

            start_time = time.time()
            mse_total = 0
            with torch.no_grad():
                for graph in test_loader:
                    graph = graph.cuda()
                    if cfg.inr.model_type == 'vanilla_mlp':
                        pred = inr_model(graph.input.cuda()).detach().cpu()* dataset.coef_norm['std'] + dataset.coef_norm['mean']
                        target = graph.output.detach().cpu()* dataset.coef_norm['std'] + dataset.coef_norm['mean']
                    else:    
                        if cfg.inr.global_norm:
                            pred = inr_model.modulated_forward(graph.input.cuda(), graph.cond.cuda()).detach().cpu() * (dataset.coef_norm['max'] - dataset.coef_norm['min']) + dataset.coef_norm['min']
                            target = (graph.output * (dataset.coef_norm['max'] - dataset.coef_norm['min']) + dataset.coef_norm['min']).detach().cpu()
                        else:
                            pred = inr_model.modulated_forward(graph.input.cuda(), graph.cond.cuda()).detach().cpu() * dataset.coef_norm['std'] + dataset.coef_norm['mean']
                            target = graph.output.detach().cpu() * dataset.coef_norm['std'] + dataset.coef_norm['mean']

                    mse = ((pred - target)**2).mean(axis=(0))
                    mse_total += mse * len(graph)

                    if num_points is None:  # Append results for full resolution
                        result_test['predictions'][target_field].append(pred.numpy())
                        result_test['targets'][target_field].append(target.numpy())
                        result_test['mse'][target_field].append(mse)
                        result_test['cond'][target_field].append(graph.cond.cpu().numpy())

            torch.cuda.empty_cache()  # Clear unused memory
            inference_time = time.time() - start_time
            # After computing the total MSE for each feature
            if isinstance(mse_total, torch.Tensor):
                mse_total = mse_total.cpu().numpy() 
            average_mse = mse_total / len(test_dataset)
            # Format each feature's MSE
            mse_strings = ", ".join([f"{mse:.4f}" for mse in average_mse])  # Create a string for each feature's MSE
            inference_time_per_sample = inference_time / len(test_dataset)

            resolution_label = 'Full' if num_points is None else num_points
            file.write(f"{resolution_label}, {inference_time_per_sample:.4f}, {mse_strings}\n")
            
            if num_points is None:  # Save the result_test dictionary only for full resolution
                outputs_save_path = os.path.join(results_directory, 'results_test_full_resolution.pt')
                torch.save(result_test, outputs_save_path)
                print(f"Full resolution test results saved to {outputs_save_path}")

    return test_loss_in


if __name__ == "__main__":
    main()