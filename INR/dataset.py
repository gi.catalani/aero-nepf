import numpy as np
import pyvista as pv
import os.path as osp
import os
import torch
import json
import matplotlib.pyplot as plt
from tqdm import tqdm
from tqdm import tqdm
from torch.utils.data import Dataset
import torch
import numpy as np
from torch_geometric.data import Data
import os

class TransonicRAE(Dataset):
    def __init__(self, data_directory, target_field, num_points=None, global_norm=False, coef_norm=None, add_cond_in=False):
        super(TransonicRAE, self).__init__()
        self.dataset = []
        self.graph_dataset = []
        self.global_norm = global_norm
        self.coef_norm = coef_norm
        self.add_cond_in = add_cond_in
        self.num_points = num_points

        print("Processing dataset...")
        self.process_data(data_directory, target_field, num_points)
        
    def process_data(self, data_directory, target_field, num_points):

        print('Loading raw data')
        db_random = np.load(os.path.join(data_directory, 'db_random.npy'), allow_pickle=True).item()
        db_cyc = np.load(os.path.join(data_directory, 'db_cyc.npy'), allow_pickle=True).item()
        
        # Merge db_random and db_cyc
        db = {key: np.concatenate((db_random[key], db_cyc[key]), axis=0) for key in ['Pressure','Xcoordinate','Ycoordinate','Vinf','Alpha','idx','Vx','Vy']}
        db['all_outputs'] = np.stack((db['Pressure'], db['Vx'], db['Vy']), axis=-1)
        print('Raw data Loaded, normalizing data')
    
        alfa = 2*(db['Alpha'] - db['Alpha'].min((0)))/ (db['Alpha'].max((0)) - db['Alpha'].min((0))) -1
        Vinf = 2*(db['Vinf'] - db['Vinf'].min((0))) / (db['Vinf'].max((0)) - db['Vinf'].min((0))) -1

        self.coef_norm = {'mean': None, 'std': None, 'min': None, 'max': None} 
        mean_out = db[target_field].mean(axis=(0,1))
        std_out = db[target_field].std(axis=(0,1))
        db[target_field] = (db[target_field]- mean_out)/std_out
        self.coef_norm['mean'] = mean_out
        self.coef_norm['std'] = std_out
        
          # Store raw data components
        if self.global_norm:
           min_out = db[target_field].min(axis=(0,1))
           max_out = db[target_field].max(axis=(0,1))
           db[target_field] = (db[target_field]- min_out)/(max_out - min_out)
           self.coef_norm['min'] = min_out
           self.coef_norm['max'] = max_out


        for idx in range(len(db['idx'])):
            if db['Vinf'][idx] / 347 >= 0.2:
                X_coord = 2*(db['Xcoordinate'][idx] - db['Xcoordinate'][idx].min()) / (db['Xcoordinate'][idx].max() - db['Xcoordinate'][idx].min()) - 1
                Y_coord = 2*(db['Ycoordinate'][idx] - db['Ycoordinate'][idx].min()) / (db['Ycoordinate'][idx].max() - db['Ycoordinate'][idx].min()) - 1
                pos = torch.tensor(np.stack((X_coord, Y_coord), axis=1), dtype=torch.float)
                output = torch.tensor(db[target_field][idx], dtype=torch.float).unsqueeze(-1) if target_field != 'all_outputs' else torch.tensor(db['all_outputs'][idx], dtype=torch.float)
                cond_norm = torch.tensor(np.array([[alfa[idx], Vinf[idx]]] * pos.shape[0]), dtype=torch.float)
                cond = torch.tensor(np.array([[db['Alpha'][idx], db['Vinf'][idx]]]), dtype=torch.float)
                
                if self.add_cond_in:
                    input_data = torch.cat((pos, cond_norm), dim=1)
                else:
                    input_data = pos

                self.dataset.append({'pos': pos, 'input': input_data, 'output': output, 'cond': cond_norm})

        self.graph_dataset = [Data(pos=d['pos'], input=d['input'], output=d['output'], cond=d['cond']) for d in self.dataset]
        if self.num_points is not None:
            self.graph_dataset = subsample_dataset(self.graph_dataset, self.num_points)
    
    def create_splits(self, train_ratio=0.8, val_ratio=0.1, test_ratio=0.1, seed=42):
        if seed is not None:
            np.random.seed(seed)
        
        total_samples = len(self.graph_dataset)
        indices = np.arange(total_samples)
        np.random.shuffle(indices)
        
        train_end = int(train_ratio * total_samples)
        val_end = train_end + int(val_ratio * total_samples)
        
        train_indices = indices[:train_end]
        val_indices = indices[train_end:val_end]
        test_indices = indices[val_end:]
        
        self.train_dataset = [self.graph_dataset[i] for i in train_indices]
        self.val_dataset = [self.graph_dataset[i] for i in val_indices]
        self.test_dataset = [self.graph_dataset[i] for i in test_indices]


    def __getitem__(self, index):

        return self.graph_dataset[index]

    def __len__(self):
        return len(self.graph_dataset)
    
def subsample_dataset(dataset, num_points):
    subsampled_dataset = []
    for i in range(len(dataset)):
        graph = dataset[i]
        if len(graph.input) > num_points:
            subsample_indices = np.random.choice(len(graph.input), num_points, replace=False)
            # Prepare subsampled attributes
            subsampled_data = {
                'input': graph.input[subsample_indices],
                'output': graph.output[subsample_indices],
                'pos': graph.pos[subsample_indices],
                'cond': graph.cond[subsample_indices]
            }
            # Create and add a new Data object to the subsampled dataset list
            subsampled_dataset.append(Data(**subsampled_data))
        else:
            # If no subsampling is needed, add the original graph to the subsampled dataset list
            subsampled_dataset.append(graph)
    
    return subsampled_dataset
            


if __name__ == "__main__":
    data_directory = '/home/dmsm/gi.catalani/Projects/RAE_Transonic/data'
    save_directory = '/home/dmsm/gi.catalani/Projects/RAE_Transonic/data'
    batch_size = 32
    target_field = 'Pressure'
    # Create dataset objects
    train_dataset_obj = TransonicRAE(data_directory, target_field, add_cond_in=False,num_points=5000)
    
    idx=100
    # Extract x and y from the dataset
    graph = train_dataset_obj.graph_dataset[idx]
    # Create the scatter plot
    plt.figure()
    plt.scatter(graph.input[:, 0].numpy(), graph.input[:, 1].numpy(), c=graph.output[:, 0].numpy(), cmap='viridis',s = 0.5)
    plt.colorbar(label='y[:, 2] value')
    plt.xlabel('x[:, 0]')
    plt.ylabel('x[:, 1]')
    plt.title(f'Scatter plot for dataset index {idx}')
    plt.show()



    
   

    